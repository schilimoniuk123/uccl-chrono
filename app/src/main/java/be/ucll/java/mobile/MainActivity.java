package be.ucll.java.mobile;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private long starttime = 0;
    TextView txt;
    private static final String TAG = "main";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt = findViewById(R.id.ChronoTxt);
        txt.setText("00:00");
        findViewById(R.id.btnStop).setEnabled(false);
    }

    public void handleClickStartButton(View view)
    {
        starttime = SystemClock.elapsedRealtime();
        txt.setText(R.string.chronoRuns);
        findViewById(R.id.btnStart).setEnabled(false);
        findViewById(R.id.btnStop).setEnabled(true);
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void handleClickStopButton(View view)
    {
        long durration = SystemClock.elapsedRealtime() - starttime;

        long durration_sec = durration/1000;
        long durration_min = durration_sec/60;
        String result = String.format("%02d" , durration_min) + ":" + String.format("%02d" , durration_sec);

        txt.setText(result);
        findViewById(R.id.btnStop).setEnabled(false);

    }
}